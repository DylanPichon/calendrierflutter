import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CustomEventButton extends StatelessWidget {
  final String _title;
  final String _hour;
  final Icon _iconLeft;
  final Icon _iconRight;
  CustomEventButton(this._title, this._iconLeft, this._iconRight, this._hour);

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Container(
          height: 36.0,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.all(Radius.circular(12)),
          ),
          child: Row(
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: _iconLeft,
              ),
              Text(
                _title,
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 16.0,
                  fontWeight: FontWeight.w600,
                ),
              ),
              Spacer(),
              Text(
                _hour,
                style: TextStyle(
                  color: Colors.grey,
                  fontSize: 16.0,
                  fontWeight: FontWeight.w600,
                ),
              ),
              _iconRight,
            ],
          ),
        ),
      ),
    );
  }
}
