import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'custom_event_button.dart';

class CustomCalendarListview extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: ListView(
        children: [
          CustomEventButton(
            "To pet Richard",
            Icon(Icons.circle, color: Colors.green),
            Icon(Icons.arrow_forward_ios, color: Colors.grey),
            "10:30 AM",
          ),
          CustomEventButton(
            "Water the flower",
            Icon(Icons.circle, color: Colors.green),
            Icon(Icons.arrow_forward_ios, color: Colors.grey),
            "12:00 AM",
          ),
          CustomEventButton(
            "Branch with Alice",
            Icon(Icons.circle, color: Colors.red),
            Icon(Icons.arrow_forward_ios, color: Colors.grey),
            "2:00 PM",
          ),
          CustomEventButton(
            "Go to the exhibition",
            Icon(Icons.circle, color: Colors.orangeAccent),
            Icon(Icons.arrow_forward_ios, color: Colors.grey),
            "3:00 PM",
          ),
        ],
      ),
    );
  }
}
