import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CustomFabButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
      child: Icon(Icons.add),
      onPressed: () {},
    );
  }
}
