import 'package:calendrier/widget/custom_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'custom_category_listview.dart';
import 'custom_main_title.dart';
import 'custom_sub_title.dart';

class CustomHeader extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.only(
            bottomRight: Radius.elliptical(800, 300),
            bottomLeft: Radius.elliptical(500, 50)),
        color: Colors.blueAccent,
      ),
      child: Column(
        children: [
          Row(
            children: [
              CustomMainTitle("Let's plan"),
              Spacer(),
              Padding(
                padding: const EdgeInsets.all(15.0),
                child: CustomImage(),
              ),
            ],
          ),
          CustomSubTitle("My schedule"),
          CustomCategoryListview(),
        ],
      ),
    );
  }
}
