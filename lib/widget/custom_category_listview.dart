import 'package:calendrier/widget/custom_category_button.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CustomCategoryListview extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        height: 70,
        child: ListView(
          scrollDirection: Axis.horizontal,
          children: [
            CustomCategoryButton(
              "Vacation",
              Icon(
                Icons.wb_sunny,
                color: Colors.blueAccent,
              ),
              "12",
            ),
            CustomCategoryButton(
              "Grocery",
              Icon(
                Icons.shopping_cart_outlined,
                color: Colors.deepOrange,
              ),
              "24",
            ),
            CustomCategoryButton(
              "Week-end",
              Icon(
                Icons.home_outlined,
                color: Colors.pinkAccent,
              ),
              "3",
            ),
          ],
        ),
      ),
    );
  }
}
