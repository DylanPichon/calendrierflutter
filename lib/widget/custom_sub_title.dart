import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CustomSubTitle extends StatelessWidget {
  final String _subTitle;
  const CustomSubTitle(this._subTitle);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Container(
        height: 40,
        child: Row(
          children: [
            Text(
              _subTitle,
              style: TextStyle(
                color: Colors.white,
                fontSize: 20.0,
                fontWeight: FontWeight.w600,
              ),
              textAlign: TextAlign.left,
            ),
          ],
        ),
      ),
    );
  }
}
