import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CustomImage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return CircleAvatar(
      radius: 25,
      backgroundImage: NetworkImage(
        "https://www.ouestfrance-emploi.com/sites/default/files/styles/610-largeur/public/metier-pere-noel_0.jpg?itok=TTPM_6Fl",
      ),
    );
  }
}
