import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CustomMainTitle extends StatelessWidget {
  final String _mainTitle;
  const CustomMainTitle(this._mainTitle);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Container(
        height: 80,
        child: Row(
          children: [
            Text(
              _mainTitle,
              style: TextStyle(
                color: Colors.white,
                fontSize: 35.0,
                fontWeight: FontWeight.w600,
              ),
              textAlign: TextAlign.left,
            ),
          ],
        ),
      ),
    );
  }
}
