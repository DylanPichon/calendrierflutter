import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CustomTimeBar extends StatelessWidget {
  final _fontSize = 20.0;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        children: [
          Text(
            "Today",
            style: TextStyle(
              color: Colors.blue,
              fontSize: _fontSize,
              decoration: TextDecoration.underline,
              decorationThickness: 2,
            ),
          ),
          Spacer(),
          Text(
            "Week",
            style: TextStyle(
              color: Colors.grey,
              fontSize: _fontSize,
            ),
          ),
          Spacer(),
          Text(
            "Month",
            style: TextStyle(
              color: Colors.grey,
              fontSize: _fontSize,
            ),
          ),
          Spacer(),
          Spacer(),
        ],
      ),
    );
  }
}
