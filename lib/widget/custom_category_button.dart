import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CustomCategoryButton extends StatelessWidget {
  final String _title;
  final String _nbItem;
  final Icon _icon;
  CustomCategoryButton(this._title, this._icon, this._nbItem);
  @override
  Widget build(BuildContext context) {
    return Card(
      child: Center(
        child: Container(
          height: 48.0,
          width: 130,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.all(Radius.circular(12)),
          ),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Spacer(),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [_icon],
              ),
              Spacer(),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    _title,
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 16.0,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  Text(
                    '$_nbItem items',
                    style: TextStyle(
                      color: Colors.grey,
                      fontSize: 13.0,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                ],
              ),
              Spacer()
            ],
          ),
        ),
      ),
    );
  }
}
