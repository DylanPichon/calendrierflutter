import 'package:calendrier/widget/custom_calendar_listview.dart';
import 'package:calendrier/widget/custom_fab_button.dart';
import 'package:calendrier/widget/custom_header.dart';
import 'package:calendrier/widget/custom_time_bar.dart';
import 'package:flutter/material.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Column(
          children: [
            CustomHeader(),
            CustomTimeBar(),
            CustomCalendarListview(),
            Padding(
              padding: const EdgeInsets.all(50.0),
              child: CustomFabButton(),
            ),
          ],
        ),
      ),
    );
  }
}
